model = keras.models.load_model('model_02')

test_dir = "./for_test"
test_images = os.listdir(f'{test_dir}')

for image in test_images:
  class_names = ['cat', 'dog']
  img_height = 180
  img_width = 180

  img = tf.keras.utils.load_img(
      f"{test_dir}/{image}", target_size=(img_height, img_width)
  )
  img_array = tf.keras.utils.img_to_array(img)
  img_array = tf.expand_dims(img_array, 0) # Create a batch
  predictions = model.predict(img_array)
  score = tf.nn.softmax(predictions[0])
  print(f"image {image} looks like {class_names[np.argmax(score)]} with {100 * np.max(score)} probability")
